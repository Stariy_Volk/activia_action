/**
 * Created by vatiba01 on 30.12.2014.
 */
var formComputing = function(form){
    var hash = '',
        email = form.find("[type='email']"),
        emailText = '',
        hiddenList = form.find("[type='hidden']");

    function formData(){
        hiddenList = form.find("[type='hidden']");
        emailText = email.val();
        hash = '';
        hiddenList.each(function(ind){
            hash += $(this).val();
        });
        return {
            'email': emailText,
            'hash': hash
        }
    }

    function openError(title, text){
        popup("info", title, text);
    }

    function checkErrors(data){
        var error = true,
            msg = '';
        if(data.hash.length < 28){
            msg = "Необходимо заполнить весь календарь картинками активностей";
            error = false;
        }
        if(data.email.length < 4){
            msg = "Необходимо заполнить почтовый адрес";
            error = false;
        }
        if(!error){
            openError("Ошибка", msg);
        }
        return error
    }

    return {
        init: function(){
            form.on("submit", function(e){
                e.preventDefault();
                var data = formData();
                if(checkErrors(data)){
                    $.ajax({
                        url: "/14day/email.php",
                        data: data,
                        type: "post",
                        success: function(response){
                            if(response.status == 'success'){
                                openError("Поздравляем!", response.data);
                                //location.reload();
                                //window.location.reload();
                                document.location.hash = data.hash;
                                setTimeout(function(){
                                    location.reload();
                                }, 5000);
                            } else {
                                openError("Ошибка", response.data);
                            }
                        }
                    });
                }
            });
        }
    }
};



(function(){
    var myForm = new formComputing($(".calendar_form").closest("form"));
    setTimeout(function(){
        myForm.init();
    }, 800);
})();