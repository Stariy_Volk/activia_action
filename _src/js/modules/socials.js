/**
 * Created by vatiba01 on 31.12.2014.
 */

(function(){
    var popups = function(url) {
        window.open(url,'','toolbar=0,status=0,width=626,height=436');
    };

    function share(){
        var tmp = 0,
            url = '',
            param = {
                purl: document.location,
                ptitle: "",
                text: "",
                img: ""
            };

        if(document.location.hash.replace("#","").split("/")[1]){
            param.purl = document.location.origin + document.location.pathname + "?" + document.location.hash.replace("#","").split("/")[0] + '&' + document.location.hash.replace("#","").split("/")[1] + document.location.hash;
        } else {
            param.purl = document.location.origin + document.location.pathname + "?" + document.location.hash.replace("#","").split("/")[0] + document.location.hash;
        }

        console.log(param.purl);

        return {
            vkontakte: function() {
                url  = 'http://vk.com/share.php?';
                url += 'url='          + encodeURIComponent(param.purl);
                url += '&title='       + encodeURIComponent(param.ptitle);
                url += '&description=' + encodeURIComponent(param.text);
                url += '&image=' + encodeURIComponent(param.img);
                url += '&noparse=true';
                popups(url);
            },
            odnoklassniki: function() {
                url  = 'http://www.odnoklassniki.ru/dk?st.cmd=addShare&st.s=1';
                url += '&st.comments=' + encodeURIComponent(param.text);
                url += '&st._surl='    + encodeURIComponent(param.purl);
                popups(url);
            },
            facebook: function() {
                url  = 'http://www.facebook.com/sharer.php?s=100';
                url += '&p[url]='       + encodeURIComponent(param.purl);
                //                url += '&p[url]='       + encodeURIComponent('http://riderosan.com');
                url += '&p[images][0]=' + encodeURIComponent(param.img);
                url += '&p[title]='     + encodeURIComponent(param.ptitle);
                url += '&p[summary]='   + encodeURIComponent(param.text);
                popups(url);
//                console.log("http://"+document.location.hostname+"?link=/"+document.location.hash.replace("#!/","")+"/"+document.location.hash);
            },
            twitter: function() {
                url  = 'http://twitter.com/share?';
                url += 'text='      + encodeURIComponent(param.text);
                url += '&url='      + encodeURIComponent(param.purl);
                url += '&counturl=' + encodeURIComponent(param.purl);
                popups(url);
            }
        }
    }

    $("body").on("click", "a.social_link", function(e){
        e.preventDefault();
        var num = $(this).closest(".social_item").index();
        if(num == 0){
            share().vkontakte();
        } else if(num == 1){
            share().facebook();
        } else if(num == 2){
            share().odnoklassniki();
        }
    });
})();