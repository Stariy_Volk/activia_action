/**
 * Created by vatiba01 on 30.12.2014.
 */

var popup = function(type, title, text, image, socs){
    var tpl = '',
        popup = $(".popup"),
        container = popup.find(".popup_container");
    var curTitle = title || "",
        curText = text || '',
        curImage = image || '',
        curSocs = socs || '';
    if(type == 'image'){
        tpl = '' +
        '<section class="popup_info_wrap active">' +
            '<div class="popup_title">' +
                '<span class="otrada">'+ curTitle +'</span>' +
            '</div>' +
            '<a class="popup_close" href="#">X</a>' +
            '<div class="popup_image">' +
                '<img src="'+ curImage +'" alt=""/>' +
            '</div>'+
            '<div class="popup_text">' +
                 curText +
            '</div>' +
            '<div class="popup_soc">' +
                '<ul class="social_list">' +
                    '<li class="social_item">' +
                        '<a class="social_link" href="#">' +
                            '<img src="img/calendar/icon_vk_green.png" alt=""/>' +
                        '</a>' +
                    '</li>' +
                    '<li class="social_item">' +
                        '<a class="social_link" href="#">' +
                            '<img src="img/calendar/icon_fb_green.png" alt=""/>' +
                        '</a>' +
                    '</li>' +
                    '<li class="social_item">' +
                        '<a class="social_link" href="#">' +
                            '<img src="img/calendar/icon_ok_green.png" alt=""/>' +
                        '</a>' +
                    '</li>' +
                    '<li class="social_item-text ">' +
                        '<span class="social_link">' +
                            'Поделись' +
                        '</span>' +
                    '</li>' +
                '</ul>' +
            '</div>' +
        '</section>';
    } else {
        tpl = '' +
        '<section class="popup_info_wrap active">' +
            '<div class="popup_title">' +
                '<span class="otrada">'+ curTitle +'</span>' +
            '</div>' +
            '<a class="popup_close" href="#">X</a>' +
            '<div class="popup_text">' +
                curText +
            '</div>' +
        '</section>';
    }
    container.html(tpl);
    container.find(".popup_close").on("click", function(e){
        e.preventDefault();
        popup.hide();
        if(type == 'image'){
            var hash = document.location.hash,
                hashSplit = hash.split("/");
            if(hashSplit.length > 1){
                document.location.hash = hashSplit[0];
            } else {
                document.location.hash = '';
            }
        }
    });
    popup.fadeIn();
};