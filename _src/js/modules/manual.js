/**
 * Created by vatiba01 on 12.01.2015.
 */

var Manual = function(){
    function popupConstructor(){
        var manPopup = {},
            base = $(".maincontent");
        manPopup.html = "" +
        '<div class="man">' +
            '<div class="man_gal"><img src="img/manual/gal.png" alt=""/></div>' +
            '<div class="man_cal"><img src="img/manual/cal.png" alt=""/></div>' +
            '<div class="man_share"><img src="img/manual/share.png" alt=""/></div>' +
            '<div class="man_submit"><img src="img/manual/submit.png" alt=""/></div>' +
            //'<a class="man_close" href="#"><img src="img/manual/close.png" alt=""/></a>' +
        '</div>';
        manPopup.obj = $(manPopup.html);
        base.append(manPopup.obj);
        return manPopup
    }

    return {
        init: function(){
            var popCon = popupConstructor();
            popCon.obj.on("click", function(e){
                e.preventDefault();
                popCon.obj.fadeOut();
            });
        }
    }
};

(function(){
    var manual = new Manual;
    setTimeout(function(){
        manual.init();
    }, 500);
})();