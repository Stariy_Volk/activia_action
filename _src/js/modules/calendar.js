/**
 * Created by vatiba01 on 26.12.2014.
 */

function Calendar(){
    var pics = $(".gallery_img"),
        items = $(".calendar_item"),
        bg = $(".calendar_bg"),
        itemsData = [],
        borders = {},
        pageHash = document.location.hash;

    items.each(function(ind){
        var data = $(this).offset();
        $(".calendar_form").append("<input type='hidden' value='' name='day"+ (ind+1) +"'>");
    });

    borders.left = bg.offset().left;
    borders.top = bg.offset().top;
    borders.right = bg.offset().left + bg.width();
    borders.bottom = bg.offset().top + bg.height();

    var timeOut = setTimeout(function(){
        items.each(function(ind){
            itemsData[ind] = $(this).offset();
        });
    }, 30);
    $(window).resize(function(){
        clearTimeout(timeOut);
        timeOut = setTimeout(function(){
            items.each(function(ind){
                itemsData[ind] = $(this).offset();
            });
            borders.left = bg.offset().left;
            borders.top = bg.offset().top;
            borders.right = bg.offset().left + bg.width();
            borders.bottom = bg.offset().top + bg.height();
        }, 350);
    });

    function findCell(x, y, callback){
        var cell = -1;
        if(x > borders.left && x < borders.right && y > borders.top && y < borders.bottom){
            for(var i = 0; i < itemsData.length; i++){
                if(x > itemsData[i]["left"] && y > itemsData[i]["top"]){
                    cell = i;
                }
            }
        } else {
            cell = -1;
        }
        if(callback && (cell+1)){
            callback(cell);
        }
    }

    function loadHash(hash){
        var curHash = hash.replace("#",""),
            cell = 0;
        if(curHash.length >= 28){
            var hashParts = curHash.split("/"),
                imagesHash = hashParts[0],
                img = hashParts[1],
                num = 0;

            var elem = $(".gallery_item").eq(num);

            for(var i = 0; i < imagesHash.length/2; i++){
                cell = i;
                var cellItem = items.eq(cell);
                num = imagesHash[i*2]+imagesHash[i*2+1];
                if(num[0]=='0'){
                    num = parseInt(num.replace('0',''));
                }
                num = $(".gallery_item").length - num;
                elem = $(".gallery_item").eq(num);
                cellItem.attr('data-item',num);
                cellItem.find(".calendar_item_wrap").html("<img src='"+ elem.find(".gallery_img img").attr("src") +"'>");
                cellItem.find(".calendar_item_text").remove();
                cellItem.append('' +
                '<span class="calendar_item_text">' +
                    '<span><span>'+elem.find(".gallery_text").text()+'</span></span>' +
                '</span>' +
                '');

                cellItem.on("click", function(e){
                    e.preventDefault();

                    var glElem = $(".gallery_item").eq(parseInt($(this).attr("data-item")));

                    popup("image", glElem.find(".gallery_item_link").text(), glElem.find('.gallery_text_more').text(), glElem.find(".gallery_img img").attr("src"));
                    pageHash = pageHash.split("/")[0] + "/" + glElem.find(".gallery_img img").attr("src").split('/')[3].replace("item","").replace(".jpg","");
                    console.log(glElem);
                    document.location.hash = pageHash;
                });
            }

            if(hashParts[1] && hashParts[1].length == 2){
                var imgNum = hashParts[1],
                    dataItem = galleryData[0];
                for(var t = 0; t < galleryData.length; t++){
                    if(galleryData[t]['id'] == imgNum){
                        dataItem = galleryData[t];
                    }
                }
                popup("image", dataItem["gallery_text"], dataItem["gallery_text_full"], dataItem["gallery_item > img"]);
            }
        } else {
            $(".wrapper").addClass("view");
            for(var tmp = 0; tmp < galleryData.length; tmp++){
                if(galleryData[tmp]['id']==curHash){
                    popup("image", galleryData[tmp]["id"]+ '. ' +galleryData[tmp]["gallery_text"], galleryData[tmp]["gallery_text_full"], galleryData[tmp]["gallery_item > img"]);
                }
            }
        }
    }

    return {
        init: function(){
            pics.drag("start", function(){
                return $( this ).clone()
                    .css("opacity", .75 )
                    .appendTo( $(".calendar") );
            })
                .drag(function(e, dd){
                    $( dd.proxy ).css({
                        top: dd.offsetY,
                        left: dd.offsetX
                    });
                    findCell(dd.offsetX + $(this).width()/2, dd.offsetY + $(this).height()/2);
                })
                .drag("end", function(e, dd){
                    var elem = $(this);
                    findCell(dd.offsetX + $(this).width()/2, dd.offsetY + $(this).height()/2, function(cell){
                        items.eq(cell).find(".calendar_item_wrap").html("<img src='"+ $( dd.proxy).find("img").attr("src") +"'>");
                        items.eq(cell).find(".calendar_item_text").remove();
                        items.eq(cell).append('' +
                            '<span class="calendar_item_text">' +
                                '<span><span>'+elem.closest(".gallery_item").find(".gallery_text").text()+'</span></span>' +
                            '</span>' +
                        '');
                        items.eq(cell).off("click").on("click", function(e){
                            e.preventDefault();
                            var src = $(this).find(".calendar_item_wrap img").attr("src"),
                                srcSplit = src.split("/"),
                                lastSrc = srcSplit[srcSplit.length - 1],
                                numSrc = lastSrc.replace("item",'').replace(".jpg",'');
                            for(var t = 0; t < galleryData.length; t++){
                                if(galleryData[t]['id']==numSrc){
                                    document.location.hash = galleryData[t]["id"];
                                    popup("image", galleryData[t]["id"]+'. '+galleryData[t]["gallery_text"], galleryData[t]["gallery_text_full"], galleryData[t]["gallery_item > img"]);
                                }
                            }
                        });
                        var parentElem = elem.closest(".gallery_item"),
                            parentNum = parentElem.find(".gallery_num").text().replace(".","");
                        $(".calendar_form").find("[name='day"+ (cell+1) +"']").val(parentNum);
                    });
                    $( dd.proxy ).remove();
                });

            loadHash(pageHash);

            console.log(pageHash.length);
        }
    }
}

(function(){
    setTimeout(function(){
        var myCalendar = new Calendar();
        myCalendar.init();
    }, 330);
})();