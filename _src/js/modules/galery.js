/**
 * Created by vatiba01 on 26.12.2014.
 */

function Galery(base){
    var wrap = base.find(".gallery_list"),
        controls = base.find(".gallery_controls > a"),
        list = base.find(".gallery_list"),
        items = list.children(),
        quantity = items.length,
        active = quantity - 1,
        maxTop = base.height() - list.height();

    function slideToActive(){
        var top = -items.eq(active).position().top;
        if(top < maxTop){
            top = maxTop;
        }
        if(top > 0){
            top = 0;
        }
        wrap.css({
            top: top
        });
    }
    function slideToNum(num){
        items.removeClass("active")
            .eq(num).addClass("active");
        active = num;
        slideToActive();
        setTimeout(function(){
            maxTop = base.height() - list.height();
            slideToActive();
        }, 150);
        setTimeout(function(){
            maxTop = base.height() - list.height();
            slideToActive();
        }, 250);
        setTimeout(function(){
            maxTop = base.height() - list.height();
            slideToActive();
        }, 500);
    }

    var timeOut = setTimeout(function(){
        slideToActive();
    }, 30);
    $(window).resize(function(){
        clearTimeout(timeOut);
        timeOut = setTimeout(function(){
            slideToActive();
            maxTop = base.height() - list.height();
        }, 350);
    });
    return {
        init: function(){
            items.find(".gallery_item_link").on("click", function(e){
                e.preventDefault();
                var elem = $(this),
                    parent = elem.closest("li"),
                    num = parent.index();
                if(!parent.hasClass("active")){
                    slideToNum(num);
                }
            });
            controls.on("click", function(e){
                e.preventDefault();
                var newActive = active+1;
                if($(this).hasClass("gallery_arrow_top")){
                    newActive = active-1;
                }
                if(newActive < quantity && newActive >= 0){
                    slideToNum(newActive);
                }
            });
            setTimeout(function(){
                slideToNum(quantity - 1);
            }, 500);
        }
    }
}

(function(){
    var html = '',
        str = '';
    for(var i = galleryData.length - 1; i >= 0; i--){
        str = (i+1)+'';
        if(str.length == 1){
            str = '0'+str;
        }
        html = html +
        '<li class="gallery_item" id="gal_'+ galleryData[i]['id'] +'">' +
            '<a class="gallery_item_link" href="#">' +
                '<span class="gallery_num">'+ str +'.</span>' +
                '<span class="gallery_text">'+galleryData[i]['gallery_text']+'</span>' +
            '</a>' +
            '<div class="gallery_more">' +
                '<div class="gallery_img">' +
                    '<img src="'+galleryData[i]['gallery_item > img']+'" alt=""/>' +
                '</div>' +
                '<div class="gallery_delimetr">' +
                    '<img src="img/gallery/delimetr.png" alt=""/>' +
                '</div>' +
                '<div class="gallery_text_more">' +
                    galleryData[i]['gallery_text_full'] +
                '</div>' +
            '</div>' +
        '</li>';
    }

    $(".gallery_list").html(html);

    var myGal = new Galery($(".gallery").eq(0));
    myGal.init();
    $("window").load(function(){
        setTimeout(function(){
            $(window).resize();
        }, 500);
    });
})();